package in.apoorv003.databinding;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import in.apoorv003.databinding.databinding.ActivityMainBinding;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        final User user = new User("Test", "This text will be updated in 5 seconds by data binding");
        user.setIsFriend(false);
        binding.setUser(user);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                user.setFirstName("User");
                user.setLastName("Click Here");
            }
        }, 5000);

    }

    public void onClickFirstName(View view) {
        User user = new User("Test1", "Click Here");
        binding.setUser(user);

    }

    public void onClickLastName(View view) {
        User user = new User("Click here", "User2");
        user.setIsFriend(true);
        binding.setUser(user);

    }


}
